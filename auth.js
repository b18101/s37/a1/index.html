// [SECTION] DEPENDENCIES
const jwt = require('jsonwebtoken');
const secret = "CourseBookingApi";

module.exports.createAccessToken = (user) => {
	console.log(user);

	// data object is created to contain some details of user
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	console.log(data);

	return jwt.sign(data, secret, {})
}

// NOTES:
/*
	1. You can only get access token when a user logs in in your app with the correct credentials

	2. As a user, you can only get your own details from your own token from logging in

	3. JWT is not meant for sensitive data.

	4. JWT is like a passport, you use around the app to access certain features meant for your type of user
*/


// goal: to check if the token exist or it is not tampered

module.exports.verify = (req, res, next) => {

	// req.header.authorization- contains jwt/ token
	let token = req.headers.authorization

	if(typeof token === "undefined"){
		// typeof result is a string

		return res.send({auth: "Failed. No Token"})

	} else {

		console.log(token);

		/*
			slice method is used for arrays and strings

			Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWY3YmJkODllMzdhMjhjNGE1NyIsImVtYWlsIjoia2FrYXNoaWhva2FnZUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NTQxMzE4OTF9.heuL9oCz2BBhavfvFUfcW_D5JehjPpAl_EQsboPPMrw
		*/

		token = token.slice(7, token.length)
		console.log(token)

		/*
			expected output:

			eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYyOTZmZWY3YmJkODllMzdhMjhjNGE1NyIsImVtYWlsIjoia2FrYXNoaWhva2FnZUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2NTQxMzE4OTF9.heuL9oCz2BBhavfvFUfcW_D5JehjPpAl_EQsboPPMrw
		*/
		
		jwt.verify(token, secret, (err, decodedToken) => {

			if(err){
				return res.send({
					auth: "Failed",
					message: err.message
				})

			} else {
				console.log(decodedToken);

				/*
				expected output: decoded token
					{
					  id: '6296fef7bbd89e37a28c4a57',
					  email: 'kakashihokage@mail.com',
					  isAdmin: false,
					  iat: 1654131891
					}
				*/

				req.user = decodedToken

				// will let us proceed to the next middleware or controller
				next();
			}

		});
	}
};

// verifying an admin

module.exports.verifyAdmin = (req, res, next) => {

	if(req.user.isAdmin){

		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}

};
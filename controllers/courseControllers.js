// [SECTION] DEPENDENCIES
const Course = require('../models/Course');

// [SECTION] CREATE COURSE
module.exports.addCourse = (req, res) => {

	console.log(req.body);

	let newCourse = new Course ({
		name: req.body.name,
		description: req.body.description,
		price: req.body.price,
	});

	newCourse.save()
	.then(course => res.send(course))
	.catch(err => res.send(err));
};

module.exports.getAllCourses = (req, res) => {

	Course.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

module.exports.getSingleCourse = (req, res) => {

	Course.findById(req.params.id)
	.then(result => res.send(result))
	.catch(err => res.send(err))
};


// [SECTION] UPDATING A COURSE

module.exports.updateCourse = (req, res) => {

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err))

};


//------------------ ACTIVITY #4 ----------------------------------

module.exports.archiveCourse = (req, res) => {

	let updates = {

		isActive: false
	}

	Course.findByIdAndUpdate(req.params.id, updates, {new:true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};

module.exports.activateCourse = (req, res) => {

	let updates = {

		isActive: true

	}

	Course.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(err => res.send(err));
};


module.exports.getActiveCourses = (req, res) => {

	let activeCourses = {
			isActive: true
		}

	Course.find(activeCourses)
	.then(result => res.send(result))
	.catch(error => res.send(error));
};
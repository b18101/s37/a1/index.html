// [SECTION] DEPENDENCIES
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const auth = require('../auth')

// [SECTION] REGISTER USER
module.exports.registerUser = (req, res) => {

	console.log(req.body);

	/*
		bcrypt.hashSync(<stringToBeHashed>, <saltRounds>)

		salt rounds- number of times the characters in the hash randomized
	
		let password = req.body.password
	*/
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User ({
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		mobileNo: req.body.mobileNo,
		password: hashedPW
	});

	newUser.save()
	.then(user => res.send(user))
	.catch(err => res.send(err));
};

// [SECTION] RETRIEVAL OF ALL USERS

module.exports.getAllUsers = (req, res) => {

	User.find({})
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

// [SECTION] LOGIN USER

module.exports.loginUser = (req, res) => {

	console.log(req.body);

	/*
		1. find the user by the email
		2. if we found user, we will check the password
		3. if we don't find the user, then we will send a message to the client
		4. if upon chekcing the found user's password is the same as our input password, we will generate the 'token/ key' to access our app. If not, we will turn them away by sending a message to the client
	*/

	User.findOne({email: req.body.email})
	.then(foundUser => {

		if(foundUser === null){
			return res.send("No user found in the database")

		} else {

			const isPassWordCorrect = bcrypt.compareSync(req.body.password, foundUser.password)
			console.log(isPassWordCorrect);

			/*
				compareSync()
				will return a boolean value
				so if it matches, this will return true
				if not, this will return false
			*/

			if(isPassWordCorrect){

				return res.send({accessToken: auth.createAccessToken(foundUser)})

			} else {

				return res.send("Incorrect password, please try again")
			}
		}
	})
	.catch(err => res.send(err));
};

// [SECTION] GETTING SINGLE USER DETAILS

module.exports.getUserDetails = (req, res) => {

	console.log(req.user);
	/*
	expected output: decoded token
		{
		  id: '6296fef7bbd89e37a28c4a57',
		  email: 'kakashihokage@mail.com',
		  isAdmin: false,
		  iat: 1654131891
		}
	*/

	// find the logged in user's document from our db and send it to the client by its id

	User.findById(req.user.id)
	.then(result => res.send(result))
	.catch(err => res.send(err));
};

module.exports.checkEmailExists = (req, res) => {

	User.findOne({email: req.body.email}).then(result => {

		console.log(result)

		if(result === null){

			res.send('Email is available')

		} else {

			res.send('Email is already registered')

		}
		
	}).catch(err => res.send(err));
};

// [SECTION] UPDATING USER DETAILS

module.exports.updateUserDetails = (req, res) => {

	console.log(req.body); // input for new values
	console.log(req.user.id); // check the logged in user's id

	let updates = {
		firstName: req.body.firstName,
		lastName: req.body.lastName,
		mobileNo: req.body.mobileNo
	}

	User.findByIdAndUpdate(req.user.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));

};

// [SECTION] UPDATE AN ADMIN

module.exports.updateAdmin = (req, res) => {

	console.log(req.user.id); // id of the logged in user

	console.log(req.params.id); // id of the user we want to update

	let updates = {

		isAdmin: true
	}

	User.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(err => res.send(err));

};
App: Booking System API
Descritpion: Allows user to enroll to a course. Allows ADMIN to perform CRUD operations on courses. Allows regular users to register.

User
firstName- string,
lastName- string,
email- string,
password- string,
mobileNo- string,
isAdmin- Boolean,
		default: false
enrollments: [
	{
		courseID: string,
		status: string,
			default: enrolled
		dateEnrolled: date
	}
]
** all values in the field given should be required, all default values should be followed

Course
name- string,
description- string,
price- number,
isActive- Boolean,
		default: true
createdOn: date
enrollees: [
	{
		userID: string,
		status: string,
		dateEnrolled: date
	}
]
** all values in the field given should be required, all default values should be followed
// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const courseControllers = require('../controllers/courseControllers');
const auth = require('../auth');

const {verify, verifyAdmin} = auth;

// [SECTION] ROUTES

// add course
router.post('/', verify, verifyAdmin, courseControllers.addCourse);

// get course
router.get('/', courseControllers.getAllCourses);

// get single course
router.get('/getSingleCourse/:id', courseControllers.getSingleCourse);

// update a course
router.put('/:id', verify, verifyAdmin, courseControllers.updateCourse);


//------------------ ACTIVITY #4 ----------------------------------


// archive a course
router.put('/archive/:id', verify, verifyAdmin, courseControllers.archiveCourse);

// activate a course
router.put('/activate/:id', verify, verifyAdmin, courseControllers.activateCourse);

// get active courses
router.get('/getActiveCourses', courseControllers.getActiveCourses);

module.exports = router;
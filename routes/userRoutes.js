// [SECTION] DEPENDENCIES
const express = require('express');
const router = express.Router();

// [SECTION] IMPORTED MODULES
const userControllers = require('../controllers/userControllers');
const auth = require('../auth');

//object destructuring from auth module
const { verify, verifyAdmin } = auth;

// [SECTION] ROUTES

// register user
router.post('/', userControllers.registerUser);

// get user
router.get('/', userControllers.getAllUsers);

// login user

router.post('/login', userControllers.loginUser);

// get user details

router.get('/getUserDetails', verify, userControllers.getUserDetails)

// check email existence
router.post('/checkEmailExists', userControllers.checkEmailExists);


// Updating user details
router.put('/updateUserDetails', verify, userControllers.updateUserDetails);

/* Mini Activity
		>> Create a user routed which is able to capture the id from its url using route params
			-- This route will only update a regular user to an admin
			-- only an admin can access this route
			-- test the route in postman
			-- send your outputs hangouts
*/

router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);


module.exports = router;
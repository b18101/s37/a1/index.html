const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	firstName: {
		type: String,
		required: [true, "User's first name is required"]
	},

	lastName: {
		type: String,
		required: [true, "User's last name is required"]
	},

	email: {
		type: String,
		required: [true, "User's email is required"]
	},

	password: {
		type: String,
		required: [true, "User's password is required"]
	},

	mobileNo: {
		type: String,
		required: [true, "User's mobile number is required"]
	},

	isAdmin: {
		type: Boolean,
		default: false
	},

	enrollments: [
		{
			courseId: {
				type: String,
				required: [true, "Course Id is required"]
			},

			status: {
				type: String,
				default: "Enrolled"
			},

			dateEnrolled: {
				type: Date,
				default: new Date()
			}
		}
	]
});

module.exports = mongoose.model("User", userSchema);